# Mapas no React com Leaflet

vamos desenvolver uma página web para demonstrar, na prática, a integração de Mapas em uma aplicação com React usando Leaflet.

## Languages
- [HTML]()
- [CSS]()
- [TypeScript]()


Alguns pontos que vamos abordar:

- Geolocalização;
- Consumo de API;
- Input com Autocomplete usando React-Select;
- Integração com Mapas;
- Estilização do Mapa e Marcadores.

Leaflet é uma biblioteca JavaScript open-source para trabalhar com Mapas em aplicações web e mobile. Pode ser simplesmente integrada a um site usando apenas HTML, CSS e JavaScript.
 
📝 Pré-requisitos
Sempre queremos entregar a melhor experiência para nossa audiência.

Para uma melhor experiência com a leitura, você precisa entender o básico de:

Como fazer requisições à API;
- React & TypeScript
- CSS
- NodeJS, Yarn ou Npm configurados e Create React App (CRA)

🔰 Iniciando o Projeto - CRA
